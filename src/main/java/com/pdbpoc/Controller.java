package com.pdbpoc;

import java.util.Optional;

import org.biojava.nbio.structure.Chain;
import org.biojava.nbio.structure.Structure;

import com.pdbpoc.service.PDBLoader;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {

  PDBLoader pdbLoader;

  @FXML
  Button loadButton;

  // Add a public no-args constructor
  public Controller() {

  }

  @FXML
  private void initialize() {
    pdbLoader = PDBLoader.getInstance();

    loadButton.setOnAction((event) -> {
      System.out.println("Starting to load the PDB file");
      Optional<Structure> protein = pdbLoader.loadPdbFile("3ruk.pdb");
      Optional<Structure> ligant1 = pdbLoader.loadPdbFile("3ruk.pdb");
      Optional<Structure> ligant2 = pdbLoader.loadPdbFile("3ruk.pdb");
      Optional<Structure> ligant3 = pdbLoader.loadPdbFile("3ruk.pdb");

      // todo alabrosk 01-10-2018: parallelize the reading of the files
      protein.ifPresent(protein1 -> {
        ligant1.ifPresent(ligant11 -> {
          ligant2.ifPresent(ligant22 -> {
            ligant3.ifPresent(ligant33 -> {
              calculate(protein1, ligant11, ligant22, ligant33);
            });
          });
        });
      });


    });
  }

  /**
   * Calculates with one pdb file
   *
   * @param protein the protein.
   * @param ligant1 the first ligant.
   * @param ligant2 the second ligant.
   * @param ligant3 the third ligant.
   */
  public void calculate(Structure protein, Structure ligant1, Structure ligant2, Structure ligant3) {
    //get values from structure and declare them
    Chain chain1 = protein.getChain("chain1");
    //... calculation

    //do some calculations

    // print the output for start
    System.out.println("calculation finished");
  }

}
