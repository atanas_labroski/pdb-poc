package com.pdbpoc.service;

import java.io.IOException;
import java.util.Optional;

import org.biojava.nbio.structure.Structure;
import org.biojava.nbio.structure.io.PDBFileReader;

/**
 * Service for loading pdb files.
 */
public class PDBLoader {

  private PDBFileReader reader;

  private static PDBLoader instance;

  private PDBLoader() {
    // private singleton constructor
    reader = new PDBFileReader();
  }

  public static PDBLoader getInstance() {
    if (instance == null) {
      instance = new PDBLoader();
    }

    return instance;
  }

  /**
   * Returns the structure of the read file or empty if it failed to read the file.
   * @param fileName the name of the file.
   *
   * @return the {@link Optional}<{@link Structure}.
   */
  public Optional<Structure> loadPdbFile(String fileName) {
    try {
      Structure structure = reader.getStructure(getClass().getClassLoader().getResource("pdb/" + fileName));
      System.out.println(structure.getId());
      return Optional.of(structure);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return Optional.empty();
  }

}
