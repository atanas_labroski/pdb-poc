package com.pdbpoc;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;

public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    URL resource = getClass().getClassLoader().getResource("sample.fxml");
    Parent root = FXMLLoader.load(resource);
    primaryStage.setTitle("Pdb example");
    MainScene scene = new MainScene(root);
    primaryStage.setScene(scene);

    // Button loadButton = (Button) scene.lookup("loadButton");
    // loadButton.setOnAction((event) -> {
    //   System.out.println("test");
    // });

    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
